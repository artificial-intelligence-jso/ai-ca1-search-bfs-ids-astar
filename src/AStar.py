from collections import Counter
import numpy as np
from queue import PriorityQueue

exec_count = 0


def is_solved(queens):
    if on_same_row_col(queens[:, 0]) or on_same_row_col(queens[:, 1]):
        return False
    for queen in queens:
        diff = abs(queens - queen)
        if on_same_diameter(diff):
            return False
    return True


def on_same_row_col(queens):
    return True if len([item for item, count in Counter(queens).items() if count > 1]) != 0 else False


def on_same_diameter(q_diff):
    for diff in q_diff:
        if diff[0] == diff[1] and diff[0] != 0:
            return True
    return False


def heuristic(queens_l, step):
    queens = np.array(queens_l)
    total = 0
    sum_row = sum(np.array(list(Counter(queens[:, 0]).values())) - 1)
    sum_column = sum(np.array(list(Counter(queens[:, 1]).values())) - 1)
    for queen in queens:
        diff = abs(queen - queens)
        for q_diff in diff:
            total += 1 if q_diff[0] == q_diff[1] and q_diff[0] != 0 else 0
    total += sum_column + sum_row + step
    return total


def take_actions(actions, queens_l, step, queue, board_size):
    queens_list = np.array(queens_l)
    global exec_count
    for index, queen in enumerate(queens_list):
        new_q_list = actions + queen
        for new_queen in new_q_list:
            exec_count += 1
            if not is_out_of_board(board_size, new_queen):
                final_list = np.copy(queens_list)
                final_list[index] = new_queen
                if is_solved(final_list):
                    return True, final_list, step + 1
                heuristic_val = heuristic(final_list.tolist(), step + 1)
                queue.put((heuristic_val, final_list.tolist(), step + 1))
    return False, [], step


def is_out_of_board(board_size, queen_pos):
    if (0 in queen_pos) or (board_size + 1 in queen_pos):
        return True
    return False


def a_star(queens, actions, board_size):
    global exec_count
    exec_count = 0
    step = 0
    q = PriorityQueue()
    heuristic_val = heuristic(queens.tolist(), step)
    q.put((heuristic_val, queens.tolist(), step))
    if is_solved(queens):
        return queens

    while not q.empty():
        queens_list = q.get()
        done, final_list, steps = take_actions(actions, queens_list[1], queens_list[2], q, board_size)
        if done:
            print('---------------results---------------')
            print('steps: ', steps - 1)
            print('exec_count: ', exec_count)
            return final_list
    return []
