import pandas as pd
import numpy as np


def print_grid(grid):
    for row in grid:
        for cell in row:
            print('X' if cell else 'O', end=' ')
        print()


def make_grid(queen_data, board_size):
    grid = np.zeros(shape=(board_size, board_size), dtype=int)
    for pair in queen_data:
        row = pair[0] - 1
        column = pair[1] - 1
        grid[row, column] = 1
    return grid


def read_input(file_name):
    queen_data = pd.read_csv(file_name, header=None).values
    return queen_data
