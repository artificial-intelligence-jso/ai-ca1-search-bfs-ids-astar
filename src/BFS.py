import math
import numpy as np
from collections import deque, Counter

exec_count = 0


def is_solved(queens):
    if on_same_row_col(queens[:, 0]) or on_same_row_col(queens[:, 1]):
        return False
    for queen in queens:
        diff = abs(queens - queen)
        if on_same_diameter(diff):
            return False
    return True


def on_same_row_col(queens):
    return True if len([item for item, count in Counter(queens).items() if count > 1]) != 0 else False


def on_same_diameter(q_diff):
    for diff in q_diff:
        if diff[0] == diff[1] and diff[0] != 0:
            return True
    return False


def take_actions(actions, queens_list, queue, board_size):
    global exec_count
    for index, queen in enumerate(queens_list):
        new_q_list = actions + queen
        for new_queen in new_q_list:
            exec_count += 1
            if not is_out_of_board(board_size, new_queen):
                final_list = np.copy(queens_list)
                final_list[index] = new_queen
                if is_solved(final_list):
                    return True, final_list
                queue.append(final_list)
    return False, []


def is_out_of_board(board_size, queen_pos):
    if (0 in queen_pos) or (board_size + 1 in queen_pos):
        return True
    return False


def bfs(queens, actions, board_size):
    q = deque()
    q.append(queens)
    if is_solved(queens):
        return queens
    while q:
        queens_list = q.popleft()
        done, final_list = take_actions(actions, queens_list, q, board_size)
        if done:
            print('---------------results---------------')
            print('steps: ', math.ceil(math.log(exec_count, 64)))
            print('exec_count: ', exec_count)
            return final_list
    return []
