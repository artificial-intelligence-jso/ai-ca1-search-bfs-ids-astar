from collections import Counter
import numpy as np

exec_count = 0


def is_solved(queens):
    if on_same_row_col(queens[:, 0]) or on_same_row_col(queens[:, 1]):
        return False
    for queen in queens:
        diff = abs(queens - queen)
        if on_same_diameter(diff):
            return False
    return True


def on_same_row_col(queens):
    return True if len([item for item, count in Counter(queens).items() if count > 1]) != 0 else False


def on_same_diameter(q_diff):
    for diff in q_diff:
        if diff[0] == diff[1] and diff[0] != 0:
            return True
    return False


def is_out_of_board(board_size, queen_pos):
    if (0 in queen_pos) or (board_size + 1 in queen_pos):
        return True
    return False


def dls(queens, actions, board_size, max_depth):
    global exec_count
    exec_count += 1
    if is_solved(queens):
        return True, queens
    if max_depth <= 0:
        return False, []

    for index, queen in enumerate(queens):
        new_q_list = actions + queen
        for new_queen in new_q_list:
            if not is_out_of_board(board_size, new_queen):
                final_list = np.copy(queens)
                final_list[index] = new_queen
                done, f_list = dls(final_list, actions, board_size, max_depth - 1)
                if done:
                    return True, f_list
    return False, []


def ids(queens, actions, board_size, max_depth):
    global exec_count
    exec_count = 0
    for i in range(max_depth):
        print(i)
        done, final_list = dls(queens, actions, board_size, i)
        if done:
            print('---------------results---------------')
            print('steps: ', i)
            print('exec_count: ', exec_count)
            return final_list
    return []
