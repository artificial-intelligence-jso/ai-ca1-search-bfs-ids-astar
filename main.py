import numpy as np
from src.BFS import bfs
from src.IDS import ids
from src.AStar import a_star
from src.Board import read_input, print_grid, make_grid
import time

BOARD_SIZE = 8


def bfs_main(test):
    queens = read_input(test)
    # print_grid(make_grid(queens, BOARD_SIZE))

    actions = np.array([[0, -1], [0, 1], [1, 0], [-1, 0], [1, 1], [-1, -1], [-1, 1], [1, -1]])
    sum_time = 0
    for i in range(3):
        start = time.time()
        bfs_res = bfs(queens, actions, BOARD_SIZE)
        sum_time += time.time() - start
    print("--- %s seconds ---" % (sum_time / 3))
    print_grid(make_grid(bfs_res, BOARD_SIZE))


def ids_main(test):
    queens = read_input(test)
    # print_grid(make_grid(queens, BOARD_SIZE))

    actions = np.array([[0, -1], [0, 1], [1, 0], [-1, 0], [1, 1], [-1, -1], [-1, 1], [1, -1]])
    sum_time = 0
    for i in range(3):
        start = time.time()
        ids_res = ids(queens, actions, BOARD_SIZE, 8)
        sum_time += time.time() - start
    print("--- %s seconds ---" % (sum_time / 3))
    print_grid(make_grid(ids_res, BOARD_SIZE))


def a_start_main(test):
    queens = read_input(test)
    # print_grid(make_grid(queens, BOARD_SIZE))

    actions = np.array([[0, -1], [0, 1], [-1, 0], [1, 0], [1, 1], [-1, -1], [-1, 1], [1, -1]])
    sum_time = 0
    for i in range(3):
        start = time.time()
        a_star_res = a_star(queens, actions, BOARD_SIZE)
        sum_time += time.time() - start
    print("--- %s seconds ---" % (sum_time / 3))
    print_grid(make_grid(a_star_res, BOARD_SIZE))


if __name__ == '__main__':
    # bfs_main('tests/test_a.csv')
    # ids_main('tests/test_a.csv')
    a_start_main('tests/test_a.csv')

